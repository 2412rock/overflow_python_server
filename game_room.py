#!/usr/bin/env python3
import asyncio
import websockets
import json,threading
from random import randint, shuffle
from threading import Thread
import time
from game import game


games = []
moves = []
CHECKER_TIME_INTERVAL = 70


def get_board(MAX, SIZE):
    NumberOf2s = randint(3, 7)
    grid = [2] * NumberOf2s + [1] * (21 - NumberOf2s) + [3] + [4]
    shuffle(grid)
    grid = [1] + grid + [1]
    return grid


async def run(websocket, path):
    created = False
    try:
        async for message in websocket:
            data = json.loads(message)
            if data['command'] == 'NUMBER_OF_ONLINE_PLAYERS':
                websocket.send(json.dumps({'number': str(len(games))}))
            elif data['command'] == 'ping':
                for element in games:
                    if element['key'] == data['key']:
                        if data['sender'] == game.get_player_a(element['instance']):
                            instance = element['instance']
                            game.update_last_ping_player_a(instance)

                        elif data['sender'] == game.get_player_b(element['instance']):
                            instance = element['instance']
                            game.update_last_ping_player_b(instance)

            elif data['command'] == 'init': #and user is valid TO DO
                # print("INIT")
                for element in games:
                    if element['key'] == data['key']:
                        created = True
                        if data['sender'] == game.get_player_a(element['instance']):
                            instance = element['instance']
                            if not game.channel_a_exists(instance):
                                game.add_channel_a(instance, websocket)
                                # print("set profile pic of a  -> ", data['profile_pic'])
                                game.set_profile_pic_a(instance, data['profile_pic'])
                                game.set_rank_a(instance, data['rank'])
                                game.set_nick_a(instance, data['nick'])
                                game.update_last_ping_player_a(element['instance'])
                                await game.send_to_a(instance, json.dumps({"board": game.get_board(instance)}))
                            else:
                                # send board together with recorded moves and turn
                                await game.send_to_a(instance, json.dumps({"board": game.get_board(instance)}))
                        elif data['sender'] == game.get_player_b(element['instance']):
                            instance = element['instance']
                            if not game.channel_b_exists(instance):
                                game.add_channel_b(instance, websocket)
                                # print("set profile pic of b  -> " ,  data['profile_pic'])
                                game.set_profile_pic_b(instance, data['profile_pic'])
                                game.set_rank_b(instance, data['rank'])
                                game.set_nick_b(instance, data['nick'])
                                game.update_last_ping_player_b(element['instance'])
                                await game.send_to_b(instance, json.dumps({"board": game.get_board(instance)}))
                                ## print("Added ")
                            else:
                                await game.send_to_b(instance, json.dumps({"board": game.get_board(instance)}))

                            # print("Sent existing board data to player b")
                        break

                if not created:
                    # print("Created game by -> ", data["sender"])
                    instance = game(data["key"], True)
                    game.add_player_a(instance, data['user_a'])
                    game.add_player_b(instance, data['user_b'])
                    game.set_turn(instance, data['user_a'])
                    board = get_board(2, 5)
                    game.set_board(instance, board)
                    if data['sender'] == data['user_a']:
                        game.add_channel_a(instance, websocket)
                        # print("set profile pic of a  -> ", data['profile_pic'])
                        game.set_profile_pic_a(instance, data['profile_pic'])
                        game.set_rank_a(instance, data['rank'])
                        game.set_nick_a(instance, data['nick'])
                        #game.update_last_ping_player_a(instance)
                        if not game.has_timestamp(instance):
                            game.add_timestamp(instance)
                        await game.send_to_a(instance, json.dumps({"board": game.get_board(instance)}))
                    elif data['sender'] == data['user_b']:
                        # print("set profile pic of a  -> ", data['profile_pic'])
                        game.set_profile_pic_b(instance, data['profile_pic'])
                        game.set_rank_b(instance, data['rank'])
                        game.set_nick_b(instance, data['nick'])
                        game.add_channel_b(instance, websocket)
                        #game.update_last_ping_player_a(instance)
                        if not game.has_timestamp(instance):
                            game.add_timestamp(instance)
                        await game.send_to_b(instance, json.dumps({"board": game.get_board(instance)}))

                    games.append({"key": data['key'], "instance": instance})

            elif data['command'] == 'move':
                # print("MOVE")
                for element in games:
                    if element['key'] == data['key']:
                        user = data['user']
                        instance = element['instance']
                        game.start_game(instance)
                        if not game.has_timestamp(instance):
                            game.add_timestamp(instance)

                        if game.get_turn(instance) == user:
                            game.add_move(instance, data['x'], data['y'])
                            if user == game.get_player_a(instance):
                                await game.send_to_b(instance, json.dumps({'x': data['x'], 'y': data['y'], 'user': user}))

                            elif user == game.get_player_b(instance):
                                await game.send_to_a(instance, json.dumps({'x': data['x'], 'y': data['y'], 'user': user}))
                            game.toggle_turns(instance)

            elif data['command'] == 'end':
                # print("End game")
                for element in games:
                    if element['key'] == data['key']:
                        instance = element['instance']
                        if data['sender'] == game.get_player_a(instance):
                            await game.send_to_b(instance, json.dumps({'stats': 'OPPONENT_LEFT'}))
                            games.remove(element)
                            # print('removed game')
                        elif data['sender'] == game.get_player_b(instance):
                            await game.send_to_a(instance, json.dumps({'stats': 'OPPONENT_LEFT'}))
                            games.remove(element)
                            # print('removed game')
                # print('UNAUTHORISED COMMAND')

            elif data['command'] == 'silent_end':
                # print("End game")
                for element in games:
                    if element['key'] == data['key']:
                        games.remove(element)
                        # print('removed game silently')
                # print('UNAUTHORISED COMMAND')

            elif data['command'] == 'pics':
                # print('Get profile pics')
                for element in games:
                    if element['key'] == data['key']:
                        instance = element['instance']
                        if data['sender'] == game.get_player_a(instance):
                            await game.send_to_a(instance, json.dumps({'opponent_pic': game.get_profile_pic_b(instance), 'nick': game.get_nick_b(instance), 'rank': game.get_rank_b(instance)}))
                            # print('sent picture of b to a')
                        elif data['sender'] == game.get_player_b(instance):
                            await game.send_to_b(instance, json.dumps({'opponent_pic': game.get_profile_pic_a(instance), 'nick': game.get_nick_a(instance), 'rank': game.get_rank_a(instance) }))

            elif data['command'] == 'rematch':
                # print('Get rematch command <<<<<<<<<<<<=====')
                for element in games:
                    if element['key'] == data['key']:
                        instance = element['instance']
                        if game.get_rematch_val(instance):
                            await game.send_to_a(instance, json.dumps(
                                {'rematch': 'rematch'}))
                            await game.send_to_b(instance, json.dumps(
                                {'rematch': 'rematch'}))
                            games.remove(element)
                        else:
                            game.rematch(instance)

            elif data['command'] == 'restart':
                # print('Sending restart')
                for element in games:
                    if element['key'] == data['key']:
                        instance = element['instance']
                        if data['sender'] == game.get_player_a(instance):
                            game.set_online_a(instance)
                            await game.send_to_a(instance, json.dumps({'restart': str(game.player_b_online(instance))}))
                            # print('sent state of b to a')
                        elif data['sender'] == game.get_player_b(instance):
                            game.set_online_b(instance)
                            await game.send_to_b(instance, json.dumps(
                                {'restart': str(game.player_a_online(instance))}))
                            # print('sent state of a to b')
    except:
        pass


async def check_online_players():
    while True:
        # print('Online checker routine')
        for element in games:
            if game.has_timestamp(element['instance']):
                if game.player_a_left(element['instance']) and game.player_b_left(element['instance']) and not \
                        game.game_started(element['instance']):
                    try:
                        await game.send_to_b(element['instance'], json.dumps({'status_opponent': 'TIME_OUT'}))
                    except:
                        pass
                    try:
                        await game.send_to_a(element['instance'], json.dumps({'status_opponent': 'TIME_OUT'}))
                    except:
                        pass
                    games.remove(element)
                elif game.player_a_left(element['instance']) and game.game_started(element['instance']):
                    try:
                        await game.send_to_b(element['instance'], json.dumps({'status_opponent': 'left'}))
                    except:
                        pass
                    games.remove(element)
                elif game.player_b_left(element['instance']) and game.game_started(element['instance']):
                    try:
                        await game.send_to_a(element['instance'], json.dumps({'status_opponent': 'left'}))
                    except:
                        pass
                    games.remove(element)

        await asyncio.sleep(3)


class Checker(Thread):
    # checks for games that have not been removed by users because \
    # they both failed to finish due to abnormal disconnect
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.start()

    def run(self):
        while True:
            # # print('ROUTINE CHECK')
            for element in games:
                if game.session_expired(element['instance']):
                    # print('REMOVED JUNK MATCH')
                    games.remove(element)
            time.sleep(CHECKER_TIME_INTERVAL)


class OnlinePlayerChecker(Thread):

    def run(self):
        asyncio.set_event_loop(asyncio.new_event_loop())
        asyncio.get_event_loop().run_until_complete(check_online_players())
        asyncio.get_event_loop().run_forever()


class MasterThread(threading.Thread):

    def run(self):
        asyncio.set_event_loop(asyncio.new_event_loop())
        asyncio.get_event_loop().run_until_complete(websockets.serve(run, '10.156.0.3', 9999))
        asyncio.get_event_loop().run_forever()


if __name__ == '__main__':
    Checker()
    op_thread = OnlinePlayerChecker()
    op_thread.start()
    master_thread = MasterThread()
    master_thread.start()

