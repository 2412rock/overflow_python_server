This repo contains the server code for the overflow game.
This game is an experiment, and therefore, there is room for many improvements

This project is made out of two parts. The matchmaking server, and the game-room server.

The matchmaking server waits for players to enter the queue (the player will send an http request). Then the client will make repeated requests to the server, to check if a match has been made. When the server succesfully matches two players, both clients will connect to the game-room server, by opening a socket connection. This is where the players moves will be streamed. When the game finishes, the clients will make a request to Firebase to update their scores based, on the game results. If a player has left mid-game, the score will be updated next time they enter the app(leaving will be marked as a loss).

The game room periodically checks whether a client has disconnected (in case of a newtwork error or if the user closed the app mid-game), and will announce the other player that the opponent has left.

The matchmaking server will also perdioacally check if a player has lost connection. If the player hasn;t sent a request in 10 seconds, it will remove the player from the queue.

