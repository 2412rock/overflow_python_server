#!/usr/bin/env python3
import uuid
from flask import Flask, jsonify
from threading import Thread
from datetime import datetime
import time
import websockets
import asyncio
import json

api = Flask(__name__)

_users = []
_matches = []
_seen_by = []
rank_max_difference = 2000
CHECKER_TIME_INTERVAL = 70
MATCH_TIMEOUT = 20
ONLINE_PLAYERS = "0"


def credentials_are_valid(usr, token):
    # MAYBE USE RSA LATER
    username = ''
    result = ''
    for char in usr:
        if char == '@':
            break
        else:
            username = username + char
    for char in username:
        if char == 'z':
            result = result + 'a'
        else:
            result = result + chr(ord(char) + 1)
    if result == token:
        return True
    else:
        return False


def rank_is_valid(user,rank):
    return True


def rank_difference_too_big(rank_1, rank_2):
    difference = float(rank_1) - float(rank_2)
    negative = True if difference < 0 else False
    difference = -difference if negative else difference
    return True if difference > rank_max_difference else False


def search_for_empty_match_slot(user,user_rank):
    for element in _matches:
        # currentElementRank > user_rank
        if element['user_a'] is not None and element['user_b'] is None and str(user) not in element['user_a']:
            if not rank_difference_too_big(element['rank_a'], user_rank):
                user_a = element['user_a']
                _matches.remove(element)
                _id = uuid.uuid4().hex

                _matches.append({'user_a': user_a, 'user_b': user, 'token': str(_id), 'seen_by_a:': 'false', 'seen_by_b':'true', 'timestamp': datetime.now()})
                print("USER: " , user + " found empty space as b")
                #print("SEEN BY + " , user)
                return jsonify({'user_a': user_a, 'user_b': user, 'token': str(_id)})

        elif (element['user_a'] in str(user) or element['user_b'] in str(user)) and element['user_b'] is not None:
            #already registered
            print("USER: ", user + " requested full match")
            user_a = element['user_a']
            user_b = element['user_b']
            token = element['token']
            element['seen_by_a'] = 'true' if user == element['user_a'] else 'false'
           #_matches.remove(element)
           # print("SEEN BY + ", user)
                #_matches.remove(element)
            return jsonify({'user_a': user_a, 'user_b': user_b, 'token': str(token)})
        elif element['user_a'] in str(user):
            return jsonify({'user_a': user, 'user_b': 'WAITING', 'token': 'NOT_GENERATED'})
    return None


def getRank(user):
    return '2300'


@api.route('/api/get_opponent/<user>/<token>')
def get_opponent(user, token):
    rank = getRank(user)
    if user == 'ENDGAME_DELETE':
        for element in _matches:
            if element['token'] == token: #TOKEN SHOULD BE REPLACED WITH KEY
                _matches.remove(element)
                return jsonify({'status': 'OK'})
    if credentials_are_valid(user, token):
        if rank_is_valid(user, rank):
            # MOVE THIS AFTER CREDENTIAL IMPLEMENTATION
            obj = search_for_empty_match_slot(user, str(rank))
            if obj is None:
                for element in _users:
                    _matches.append({'user_a': element['user'], 'user_b': None, 'rank_a': str(rank), 'timestamp': None})
                    _users.remove(element)
                    return jsonify({'status': 'STARTED_SEARCH'})
            else:
                for element in _users:
                    if element['user'] == user:
                        _users.remove(element)
                return obj
        return jsonify({'status': 'weird'})

    elif user == token:
        for element in _matches:
            #NEW CHANGE
            if (element['user_a'] == user and element['user_b'] is None) or \
                    (element['user_a'] is None and element['user_b'] == user):
                element['user_a'] = None
                element['user_b'] = None
                _matches.remove(element)
                return jsonify({'token': 'REMOVE_SUCCESS'})
            elif (element['user_a'] == user and element['user_b'] is not None) or (element['user_b'] == user and element['user_a'] is not None):
                return jsonify({'token': 'INGAME'})

        return jsonify({'token': 'ALREADY_REMOVED'})
        # _matches.remove(element)
    else:
        return jsonify({'token': 'INVALID_RANK'})


@api.route('/api/hello/<user>/<token>')
def register(user,token):
    if user == 'ONLINE_PLAYERS':
        # make socket request
        #res = await get_online_players()
        return ONLINE_PLAYERS
    if credentials_are_valid(user,token):
        for element in _users:
            if element['user'] in str(user):
                return jsonify({'status': 'ALREADY_REGISTERED'})

        _users.append({'user': str(user), 'login_token': str(token)})
        return jsonify({'status:': 'REGISTERED'})
    else:
        return jsonify({'status:': 'INVALID_CREDENTIALS'}), 200


async def get_online_players():
    uri = "ws://10.156.0.3:9999"
    async with websockets.connect(uri) as websocket:
        while True:
            with api.app_context():
                await websocket.send(json.dumps({'command': 'NUMBER_OF_ONLINE_PLAYERS'}))
            res = await websocket.recv()
            print("Got response from game_room")
            print(str(res))
            time.sleep(30)
        #return res


async def forever():
    while True:
        await get_online_players()
        time.sleep(60)


class Checker(Thread):
    # checks for matches that have not been removed by users because \
    # they both failed to finish due to abnormal disconnect
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.start()

    def run(self):
        while True:
            # print('ROUTINE CHECK')
            current = datetime.now()
            for element in _matches:
                if element['timestamp'] is not None:
                    diff = current - element['timestamp']
                    if diff.seconds > MATCH_TIMEOUT:
                        print("Found expired match")
                        _matches.remove(element)
            time.sleep(CHECKER_TIME_INTERVAL)


class MasterThread(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.start()

    def run(self):
        asyncio.set_event_loop(asyncio.new_event_loop())
        asyncio.get_event_loop().run_until_complete(get_online_players())
        asyncio.get_event_loop().run_forever()

if __name__ == '__main__':
    print('running')
    Checker()
    MasterThread()
    #loop = asyncio.get_event_loop()
    #loop.run_until_complete(forever())
    api.run(host='10.156.0.2', port=8000)

