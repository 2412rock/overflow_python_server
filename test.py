import asyncio
import websockets
import json,threading
from random import randint, shuffle
from threading import Thread
import time
import requests

class TestUser:

    email = None
    photo = None
    nick = None
    key = None
    QUEUE_HOST = "http://35.234.123.234:80/api/"
    GAME_ROOM_HOST = '35.246.237.74'

    def __init__(self,e,p,n):
        self.email = e
        self.photo = p
        self.nick = n
        self.key = self._get_key()

    def _get_key(self):
        username = ''
        result = ''
        for char in self.email:
            if char == '@':
                break
            else:
                username = username + char
        for char in username:
            if char == 'z':
                result = result + 'a'
            else:
                result = result + chr(ord(char) + 1)
        return result

    def register_to_queue(self):

        r = requests.get(url=self.QUEUE_HOST + 'hello/' + self.email + '/' + self.key)

        data = r.json()

        res = data['opponent']
        return res

    def found_opponent(self):
        r = requests.get(url=self.QUEUE_HOST + 'get_opponent/' + self.email + '/' + self.key)
        if r != 'WAITING':
            print('Found opponent')
            return True
        return False

    def init_game_socket(self):


    def go_play(self):
        key = self._get_key()
        # get opponent b=y calling host + '/' + self.email + '/' key
        while True:
            res = self.register_to_queue()
            if res == 'OK':
                print('Got in queue')
                break
        while True:
            if self.found_opponent():
                break

        initGameSocket()
        getBoard()
        sendRandomMoves()



        pass
