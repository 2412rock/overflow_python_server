
from datetime import datetime

class game:
    A_POS = {'x': 0, 'y': 0}

    verbose = False
    board = None
    GAME_DURATION = 210  # includes network delays
    ONLINE_TIMEOUT = 15
    id = ""
    user_a = ""
    user_b = ""
    nick_a = ""
    nick_b = ""
    profile_pic_a = None
    rank_a = ""
    rank_b = ""
    profile_pic_b = None
    user_a_channel = None
    user_b_channel = None
    created_at = None
    nick_a = None
    first_move_done = None
    nick_b = None
    state = ""
    last_move = ""
    rematch = None
    ping_a = None
    ping_b = None
    attempt_a = None
    attempt_b = None
    moves = None

    def __init__(self, _id, _verbose):
        self.state = "created"
        self.id = _id
        self.rematch = 0
        self.verbose = _verbose
        self.attempt_a = 0
        self.attempt_b = 0
        self.moves = []
        self.first_move_done = False

    def add_move(self, x, y):
        self.moves.append({'x': x, 'y': y})

    def mov_exists(self, x , y):
        for mov in self.moves:
            if mov['x'] == x and mov['y'] == y:
                return True
        return False

    def game_started(self):
        return self.first_move_done

    def start_game(self):
        self.first_move_done = True

    def player_a_attempt(self):
        return self.attempt_a

    def player_b_attempt(self):
        return  self.attempt_b

    def update_last_ping_player_a(self):
        self.ping_a = datetime.now()
        # print('PING A {0}'.format(str(self.ping_b)))

    def update_last_ping_player_b(self):
        self.ping_b = datetime.now()
        # print('PING B {0}'.format(str(self.ping_b)))

    def get_last_ping_player_a(self):
        return self.online_a

    def increase_online_checker_attempt_a(self):
        self.attempt_a = self.attempt_a + 1

    def increase_online_checker_attempt_b(self):
        self.attempt_b = self.attempt_b + 1

    def player_b_left(self):
        # difference between ping a and ping b should not be greater than 20
        diff = datetime.now() - self.ping_b
        # print('datetime diff between {0}'.format(str(datetime.now()), str(self.ping_b)))
        if diff.seconds > self.ONLINE_TIMEOUT:
            return True
        else:
            return False

    def player_a_left(self):
        # difference between ping a and ping b should not be greater than 20
        diff = datetime.now() - self.ping_a
        # print('datetime diff between {0}'.format(str(datetime.now()), str(self.ping_a)))
        if diff.seconds > self.ONLINE_TIMEOUT:
            return True
        else:
            return False

    def rematch(self):
        self.rematch = self.rematch + 1

    def add_timestamp(self):
        # print("Added timeStamp")
        self.created_at = datetime.now()
        self.ping_a = datetime.now()
        self.ping_b = datetime.now()

    def has_timestamp(self):
        if self.created_at is None:
            return False
        else:
            return True

    def session_expired(self):
        if self.created_at is not None:
            current = datetime.now()
            diff = current - self.created_at
            if diff.seconds > self.GAME_DURATION:
                return True
        return False

    def get_rematch_val(self):
        if self.rematch > 0:
            return True
        else:
            return False

    def set_rank_a(self,r):
        self.rank_a = r

    def set_rank_b(self,r):
        self.rank_b = r

    def get_rank_a(self):
        return self.rank_a

    def get_rank_b(self):
        return self.rank_b

    def set_nick_a(self, n):
        self.nick_a = n

    def player_a_online(self):
        return self.online_a

    def player_b_online(self):
        return self.online_b

    def set_nick_b(self, n):
        self.nick_b = n

    def add_player_a(self, _user_a):
        self.user_a = _user_a

    def set_profile_pic_a(self, url):
        self.profile_pic_a = url

    def set_profile_pic_b(self, url):
        self.profile_pic_b = url

    def get_profile_pic_a(self):
        return self.profile_pic_a

    def get_profile_pic_b(self):
        return self.profile_pic_b

    def set_board(self, board):
        self.board = board

    def get_board(self):
        return self.board

    def add_player_b(self, _user_b):
        self.user_b = _user_b

    def add_channel_a(self, channel):
        self.user_a_channel = channel
        #if self.verbose: # print("A_channel: {}".format(channel))

    def add_channel_b(self, channel):
        self.user_b_channel = channel
        #if self.verbose: # print("B_channel: {}".format(channel))

    def add_nickname_a(self, nick):
        self.nick_a = nick

    def add_nickname_b(self, nick):
        self.nick_b = nick

    def get_nick_a(self):
        return self.nick_a

    def get_nick_b(self):
        return self.nick_b

    async def send_to_a(self, message):
        await self.user_a_channel.send(message)

    async def send_to_b(self, message):
        await self.user_b_channel.send(message)

    def get_player_a(self):
        return self.user_a

    def get_player_b(self):
        return self.user_b

    def player_a_exists(self):
        if self.user_a is not None:
            return True
        else:
            return False

    def player_b_exists(self):
        if self.user_b is not None:
            return True
        else:
            return False

    def channel_a_exists(self):
        if self.user_a_channel is not None:
            return True
        else:
            return False

    def channel_b_exists(self):
        if self.user_b_channel is not None:
            return True
        else:
            return False

    def first(self, __this):
        self.state = __this
        self.last_move = self.A_POS

    def set_turn(self, _user):
        self.state = _user

    def get_turn(self):
        return self.state

    def set_last_move(self, move):
        self.last_move = move

    def get_last_x(self):
        return self.last_move['x']

    def get_last_y(self):
        return self.last_move['y']

    def toggle_turns(self):
        if str(self.state) ==  str(self.user_a):
            # print("PLAYER_B MOVES NOW")
            self.state = self.user_b
        elif str(self.state) == str(self.user_b):
            # print("PLAYER_A MOVES NOW")
            self.state = self.user_a

    def get_info(self):
        return {
            "user_a": self.user_a,
            "user_b": self.user_b,
            "match_key": self.id
        }


